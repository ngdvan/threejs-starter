import * as THREE from 'three';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
const scene = new THREE.Scene();


const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

// Thêm Camera
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
camera.position.set(0,50,50)
const controls = new OrbitControls( camera, renderer.domElement );
controls.update()

// Thêm Cube
const cubeGeometry = new THREE.BoxGeometry( 5, 5, 5 );
const texture = new THREE.TextureLoader().load('textures/Tex_0095_1.dds_baseColor.png' );
const cubeMaterial = new THREE.MeshBasicMaterial( { map:texture } );
const cube = new THREE.Mesh( cubeGeometry, cubeMaterial );
cube.position.set( 0,10,0)
cube.castShadow = true
scene.add( cube );

// Thêm ground
const groundGeometry = new THREE.PlaneGeometry(40, 40, 32, 32);
groundGeometry.rotateX(-Math.PI / 2);
const groundMaterial = new THREE.MeshBasicMaterial({color: 0x555555});
const groundMesh = new THREE.Mesh(groundGeometry, groundMaterial);
groundMesh.castShadow = false;
groundMesh.receiveShadow = true;
scene.add(groundMesh);

// Ambient light
const light = new THREE.AmbientLight( 0x404040 ); // soft white light
scene.add( light );

// White directional light at half intensity shining from the top.
const directionalLight = new THREE.DirectionalLight( 0xb9d11b, 1 );
directionalLight.position.set(0, 100, 100)
directionalLight.castShadow = true
scene.add( directionalLight );


// Spot light
const spotLight = new THREE.SpotLight(0xffffff,  3, 100, 0.22, 1);
spotLight.position.set(0, 25, 0);
spotLight.castShadow = true;
spotLight.shadow.bias = -0.0001;
scene.add( spotLight );

const loader = new GLTFLoader();

loader.load( 'model/shiba/scene.gltf', function ( gltf ) {
	const objs = gltf.scene;
	objs.position.set(0,10,0)
	objs.scale.set(100,100,100)

	scene.add( gltf.scene );

}, undefined, function ( error ) {

	console.error( error );

} );

 


function animate() {
	requestAnimationFrame( animate );

	// cube.rotation.x += 0.01;
	// cube.rotation.y += 0.01;
	controls.update();
	renderer.render( scene, camera );
}

animate();



abc 123234324324
abc 123234324324